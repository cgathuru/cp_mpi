all: program

program: cp_mpi.o
	mpicc -o cp_mpi cp_cpi.o

cp_mpi.o: cp_mpi.c
	mpicc -c cp_mpi.c

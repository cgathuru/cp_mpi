#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "math.h"
#include "mpi.h"

#define SUCCESS 0
#define ERROR -1

#define EPSILON 0.000001

void read_matrix_size_from_file(char *filename, int *rows, int *columns);
double ** read_user_matrix_from_file(char *filename, int rows, int columns, int rank, int nprocs);
double ** allocate_matrix(int, int);
void free_matrix(double **matrix, int rows);
void divide_by_max(double **, int, int);
void input_clicking_probabilities(double **, int, int, double *);
void write_clicking_probabilities_to_file(double *cp, int rows);
void print_best_acceptance_threshold(double *, int);
void print_matrix(double **, int, int);
int malloc2double(double ***array, int n, int m);
int free2ddouble(double ***array);


int main (int argc, char** argv)
{
	if (argc != 2)
	{
		printf("please provide a user matrix!\n");
		return ERROR;
	}

	/* init */
	int rank, nprocs;
	MPI_Init( &argc, &argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank);
	MPI_Comm_size( MPI_COMM_WORLD, &nprocs);
	clock_t begin1, end1, begin2, end2;

	/* setup */
	int rows, columns;
	double **A;
	double *cp;
	read_matrix_size_from_file(argv[1], &rows, &columns);
	cp = malloc(columns * sizeof(double));
	/* Each process will read its own subset of the larger matrix */
	
	
	MPI_Bcast(&rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&columns, 1, MPI_INT, 0, MPI_COMM_WORLD);


	/* Split the matrix into submatrixs */
	//int rpp = rows / rank;
	//int rrpp = rows % rank;

	int i,j,k;
	int *map = malloc(sizeof(int) * rows);

	A = read_user_matrix_from_file(argv[1], rows, columns, rank, nprocs);
  struct timeval start, end;
  gettimeofday(&start, NULL);

	for (i = 0; i < rows; i++)
	{
		map[i] = i % nprocs;
	}

	double pivot;
	for (k = 0; k < rows; k++) {
		MPI_Bcast (&(A[k][k]),columns-k,MPI_DOUBLE,map[k],MPI_COMM_WORLD);
		for (i = 0; i < rows; i++) {
			if (i == k) continue;

			if(map[i] == rank)
      {
				pivot = A[i][k] / A[k][k];

				for (j = k; j < columns; j++) {
					A[i][j] = A[i][j] - pivot*A[k][j];
				}
			}
		}
	}


	/* Back-substitution */
	int row,row2;
	for (row = rows-1; row >= 0; row--) {
		A[row][columns-1] = A[row][columns-1] / A[row][row];			
		A[row][row] = 1;
		MPI_Bcast (&(A[row][0]),columns,MPI_DOUBLE,map[row],MPI_COMM_WORLD);
	}

	divide_by_max(A, rows, columns);
  gettimeofday(&end, NULL);
  printf("\n\nAlgorithm's computational part duration : %ld\n", \
               ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)));

		/* results */
	input_clicking_probabilities(A, rows, columns, cp);
	print_best_acceptance_threshold(cp, rows);


	// if (rank == 0)
	// {
	// 	print_matrix(A, rows, columns);
	// 	divide_by_max(A, rows, columns);
		/* results */
		// input_clicking_probabilities(A, rows, columns, cp);
		// print_best_acceptance_threshold(cp, rows);
		// write_clicking_probabilities_to_file(cp, rows);
		// printf("Free cp process %d\n", rank);
		
		// printf("Freed cp\n");
	//}
	free(cp);
	/* results */
	//free_matrix(A, rows);
	free2ddouble(&A);
	MPI_Finalize();
	return SUCCESS;

}


void read_matrix_size_from_file(char *filename, int *rows, int *columns)
{
	FILE *file;
	file = fopen(filename, "r");

	/* get number of rows and columns*/
	*rows = 1;
	*columns = 1;
	char c;
	int columns_known = 0;
	while(!feof(file)) {
		c = fgetc(file);
		if (c == ' ') {
			if (!columns_known) (*columns)++;
		} 

		if (c == '\n') {
			(*rows)++;
			columns_known = 1;
			continue;
		}
	}


	fclose(file);
}

double ** read_user_matrix_from_file(char *filename, int rows, int columns, int rank, int nprocs)
{
	FILE *file;
	file = fopen(filename, "r");

	/* read values into an array */
	//rewind(file);
	//double **matrix = allocate_matrix(rows, columns);
	double **matrix;
	malloc2double(&matrix, rows, columns);
	int i,j;
	int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	int rank_start, rank_increment;
	double buf [columns];
	for (i = 0; i < rows; i++)
	{
		if (world_rank == i % nprocs)
		{
			for (j = 0; j < columns; j++)
			{
				fscanf(file, "%lf", &matrix[i][j]);
			}
		}else {
			for ( j= 0; j < columns; j++)
			{
				fscanf(file, "%lf", &buf[j]);
				matrix[i][j] = 0;
			}
		}
		
	}
	fclose(file);

	//print_matrix(matrix, rows, columns);

	return matrix;
}

double ** allocate_matrix(int rows, int columns)
{
	double ** matrix = (double **) malloc(sizeof(double *) * rows);
	int i;
	for (i = 0; i < rows; i++)
	{
		matrix[i] = (double *) malloc(sizeof(double) * columns);
	}

	return matrix;
}

void free_matrix(double **matrix, int rows)
{
	int i;
	for (i = 0; i < rows; i++)
	{
		free(matrix[i]);
	}
	free(matrix);
}

void input_clicking_probabilities(double **matrix, int rows, int columns, double *cp) {
	int row;
	for (row = 0; row < rows; row++) {
		cp[row] = matrix[row][columns-1];
	}
}

void write_clicking_probabilities_to_file(double *cp, int rows)
{
	/* write clicking probabilities to file */ 
	FILE *output_file;
	int row;
	output_file = fopen("clicking_probabilities.txt","w");
	for (row = 0; row < rows; row++) {
		fprintf(output_file, "%lf\n", cp[row]);
	}

	fclose(output_file);
}

void print_matrix(double **matrix, int rows, int columns)
{
	FILE *output_file;
	int row, column;
	// Get the rank of the process
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	if (world_rank == 1)
	{
		output_file = fopen("rank1_row_reduced_matrix.txt","w");
	}
	else {
		output_file = fopen("row_reduced_matrix.txt","w");
	}
	
	for (row = 0; row < rows; row++) {
	 	for (column = 0; column < columns; column++) {
	 		fprintf(output_file,"%lf ",matrix[row][column]);
	 	}
	 	fprintf(output_file,"\n");
	}	
	fclose(output_file);
}

void print_best_acceptance_threshold(double *cp, int rows) {
/* TODO! (you didn't think this would ALL be handed to you, did you?) */
	int i,j;
	int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	
	int map[5];
	double max_profit;

	double prob[] = {0.2, 0.4, 0.6, 0.8};
	double profit[4];
  double min_cp;
  
  /* We only need 4 processors here */
  for(i =0; i < 4; i++)
  {
  	map[i] = i % nprocs;
  	if(rank == map[i])
  	{
  		profit[i] = 0;
  		for (j = 0; j < rows; j++)
  		{
  			if(cp[j] > prob[i])
        {
  				profit[i] = profit[i] + (cp[j] * 1) + ((1- cp[j]) * (-2));

        }
  		}
  		MPI_Allreduce(&(profit[i]), &max_profit, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      

  	}
  }

  for(i=0; i< 4; i++)
  {
    if (rank == map[i])
    {
      double value = prob[i];
      if (max_profit != profit[i])
      {
        value = prob[3];
        
      }
      MPI_Reduce(&value, &min_cp, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
    }
  }

  if (rank == 0)
  {
  	printf("Max profit is %G\n", max_profit);
    printf("Threshold is %G\n", min_cp);
  }
}

void divide_by_max(double **matrix, int rows, int columns) {
	double max = 0; 
	int row, column;

	/* get max so we can divide by this later to get probabilities */
	for (row = 0; row < rows; row++) {		
		if (max < fabs(matrix[row][columns-1])) max = fabs(matrix[row][columns-1]);
	}
	//printf("Max value for divide is %G\n", max);

	/* divide by max and take abs */
	for (row = 0; row < rows; row++) {
		/* check for division by zero */
		if (equals(max,0)) {
			matrix[row][columns-1] = 0;
		} else {
			matrix[row][columns-1] = fabs (matrix[row][columns-1]) / max;
		}
		
	}
}

int equals(double a, double b) {
	if (fabs(a-b) < EPSILON) return 1;
	else return 0;
}

int malloc2double(double ***array, int n, int m)
{

    /* allocate the n*m contiguous items */
    double *p = (double *)malloc(n*m*sizeof(double));
    if (!p) return -1;

    /* allocate the row pointers into the memory */
    (*array) = (double **)malloc(n*sizeof(double*));
    if (!(*array)) {
       free(p);
       return -1;
    }
    int i;
    /* set up the pointers into the contiguous memory */
    for (i=0; i<n; i++) 
       (*array)[i] = &(p[i*m]);

    return 0;
}

int free2ddouble(double ***array)
{
    /* free the memory - the first element of the array is at the start */
    free(&((*array)[0][0]));

    /* free the pointers into the memory */
    free(*array);

    return 0;
}